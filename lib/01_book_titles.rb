class Book
  attr_writer :title

  def title
    small_word = %w(a an the in and of)
    @title.split.map.with_index do |el, idx|
      if idx.zero?
        el.capitalize
      elsif small_word.include?(el)
        el
      else
        el.capitalize
      end
    end.join(" ")
  end
end
