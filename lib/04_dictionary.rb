class Dictionary
  attr_accessor :keywords, :entries, :content

  def initialize(entries = {})
    @content = {}
    @content[entries.keys[0]] = entries.values[0] unless entries.empty?
    @entries = entries
  end
  
  def keywords
    @keywords = @content.keys.sort.flatten
  end

  def add(option)
    if option.is_a?(Hash)
      @entries = option
      @keywords = option.keys
      @content[@keywords] = option.values[0]
    else
      @keywords = [option]
      @content[option] = nil
      @entries = { option => nil }
    end
    @keywords = @keywords.sort
  end

  def include?(keyword)
    return true if @content.keys.include?(keyword)
    false
  end

  def find(key)
    find_result = {}
    @content.each do |k, v|
      find_result[k[0]] = v if k[0].include?(key)
    end
    find_result
  end

  def printable
    print_result = []
    @content.each do |k, v|
      print_result << ["[#{k[0]}] \"#{v}\""]
    end
    print_result.sort.join("\n")
  end
end
