class Timer
  # initialize seconds default value of 0()
  def initialize(seconds = 0)
    @seconds = seconds
  end

  # write seconds method
  def seconds=(seconds)
    @seconds = seconds
  end

  # read seconds method
  def seconds
    @seconds
  end

  def time_string
    hr = seconds / 60 / 60
    mins = seconds / 60 % 60
    sec = seconds % 60
    "#{time_style(hr)}:#{time_style(mins)}:#{time_style(sec)}"
  end

  def time_style(num)
    if num < 10
      "0#{num}"
    else
      num.to_s
    end
  end
end
