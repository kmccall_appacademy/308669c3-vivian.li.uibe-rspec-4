class Temperature
  # TODO: your code goes here!
  def initialize(option = {})
    # @option = option
    if option[:f]
      @fahrenheit = option[:f]
    else
      @celsius = option[:c]
    end
  end

  def ftoc(temp)
    (temp - 32) * 5 / 9.0
  end

  def ctof(temp)
    temp * 9 / 5.0 + 32
  end

  def in_celsius
    if @celsius
      @celsius
    else
      ftoc(@fahrenheit)
    end
  end

  def in_fahrenheit
    if @fahrenheit
      @fahrenheit
    else
      ctof(@celsius)
    end
  end

  def self.from_fahrenheit(temp)
    new(f: temp)
  end

  def self.from_celsius(temp)
    new(c: temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end


class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
